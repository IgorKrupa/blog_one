<?php

/**
 * This is the model class for table "tbl_post_image".
 *
 * The followings are the available columns in table 'tbl_post_image':
 * @property integer $id
 * @property string $image
 * @property integer $post_id
 */
class PostImage extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_post_image';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('image, post_id', 'required'),
			array('post_id', 'numerical', 'integerOnly'=>true),
			array('image', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, image, post_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'image' => 'Image',
			'post_id' => 'Post',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('post_id',$this->post_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PostImage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    protected function beforeValidate() {

        if (isset($_FILES['post_images']))
        {
            $type = explode('/', $_FILES['post_images']['type']);

            $name = md5(time()).'.'.$type[1];

            if ( in_array($type[1], ['jpg', 'png', 'jpeg'] ) )
            {
                $path = $_SERVER['DOCUMENT_ROOT'].'/upload/images/'.$name;

                if ( move_uploaded_file($_FILES['post_images']['tmp_name'], $path) )
                    $this->image = '/upload/images/'.$name;
            }
        }

        return parent::beforeValidate();
    }

    protected function deteleImage($src)
    {
        if ( file_exists($_SERVER['DOCUMENT_ROOT'].$src) )
            unlink($_SERVER['DOCUMENT_ROOT'].$src);
    }


    protected function afterDelete()
    {
        parent::afterDelete();

        $this->deteleImage($this->image);

    }

}
