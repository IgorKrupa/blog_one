<?php

/**
 * This is the model class for table "tbl_category".
 *
 * The followings are the available columns in table 'tbl_category':
 * @property integer $id
 * @property string $title
 * @property integer $status
 * @property integer $left
 * @property integer $right
 * @property integer $level
 */
class Category extends CActiveRecord
{
    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('status, parent_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, status, parent_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'status' => 'Статус',
			'left' => 'Left',
			'right' => 'Right',
			'level' => 'Level',
            'parent_id' => 'Родительская категория'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('left',$this->left);
		$criteria->compare('right',$this->right);
		$criteria->compare('level',$this->level);
        $criteria->compare('parent_id',$this->parent_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getCategoriesList()
    {
        $categories = self::model()->findAll();
        $list = CHtml::listData($categories, 'id', 'title');

        return $list;
    }

    public static function getStatusList()
    {
        return array(
            self::STATUS_DEACTIVE => 'Не активно',
            self::STATUS_ACTIVE => 'Активно'
        );

    }

    public static function getStatus($numer)
    {
        $list = self::getStatusList();

        if ( isset($list[$numer]) )
            return $list[$numer];

        return null;
    }

    public static function getCategoryName($id)
    {
        $category = self::model()->findByPk($id);

        if ( $category !== null )
            return $category->title;

        return null;
    }

    public static function getTree()
    {

        $cats = array();
        $models = self::model()->findAllByAttributes(array('status'=>self::STATUS_ACTIVE));

        foreach ($models as $model)
        {
          $cat = array(
            'id' => $model->id,
            'parent_id' => $model->parent_id,
            'title' => $model->title
          );
          $cats_ID[$model->id][] = $cat;
          $cats[$model->parent_id][$model->id] = $cat;
        }

        return $cats;
    }

    public static function getCatIds()
    {
        $cats_ID = array();
        $models = self::model()->findAllByAttributes(array('status'=>self::STATUS_ACTIVE));

        foreach ($models as $model)
        {
            $cat = array(
                'id' => $model->id,
                'parent_id' => $model->parent_id,
                'title' => $model->title
            );
            $cats_ID[$model->id][] = $cat;
        }

        return $cats_ID;
    }


    // Поиск в дереве в обратном порядке
    public static function findParentCategory($tmp, $cur_id, &$arrCats = array())
    {

        if( isset($tmp[$cur_id][0]['parent_id']) and $tmp[$cur_id][0]['parent_id']!=0 ){

            $arrCats[] = $tmp[$cur_id][0]['parent_id'];
            return self::findParentCategory( $tmp, $tmp[$cur_id][0]['parent_id'], $arrCats );
        }

        return (int)$tmp[$cur_id][0]['id'];
    }

    public static function buildTree($cats,$parent_id,$only_parent = false)
    {
        if (is_array($cats) and isset($cats[$parent_id]))
        {
            $tree = '<ul>';
            if ($only_parent==false){
                foreach($cats[$parent_id] as $cat){
                    $tree .= '<li><a href="'.Yii::app()->createUrl("site/index",array("category"=>$cat['id'])).'">'.$cat['title'];
                    $tree .=  self::buildTree($cats,$cat['id']);
                    $tree .= '</a></li>';
                }
            }elseif (is_numeric($only_parent)){
                $cat = $cats[$parent_id][$only_parent];
                $tree .= '<li><a href="'.Yii::app()->createUrl("site/index",array("category"=>$cat['id'])).'" >'.$cat['title'];
                $tree .=  self::buildTree($cats,$cat['id']);
                $tree .= '</a></li>';
            }
            $tree .= '</ul>';
        }
        else
            return null;

        return $tree;
    }

}
