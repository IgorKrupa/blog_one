<?php

/**
 * This is the model class for table "tbl_comment".
 *
 * The followings are the available columns in table 'tbl_comment':
 * @property integer $id
 * @property integer $post_id
 * @property string $content
 * @property integer $user_id
 * @property string $date_create
 * @property integer $status
 */
class Comment extends CActiveRecord
{
    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $username_search;
    public $post_search;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('post_id, content, user_id, status, date_create', 'required'),
			array('post_id, user_id, status', 'numerical', 'integerOnly'=>true),
			array('date_create', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, post_id, content, user_id, date_create, status, username_search, post_search', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'post'  => array(self::BELONGS_TO, 'Post', 'post_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'post_id' => 'Запись',
			'content' => 'Комментарий',
			'user_id' => 'Пользователь',
			'date_create' => 'Дата создания',
			'status' => 'Статус',
            'username_search'=> 'Пользователь',
            'post_search'=>'Запись'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with = array( 'user', 'post' );
        $criteria->compare( 'user.username', $this->username_search, true );
        $criteria->compare('post.title', $this->post_search, true);
		$criteria->compare('id',$this->id);
		$criteria->compare('post_id',$this->post_id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public static function getStatusList()
    {
        return array(
            self::STATUS_DEACTIVE => 'Не активно',
            self::STATUS_ACTIVE => 'Активно'
        );

    }

    public static function getStatus($numer)
    {
        $list = self::getStatusList();

        if ( isset($list[$numer]) )
            return $list[$numer];

        return null;
    }

    public static function getPostList()
    {
        $posts = Post::model()->findAll();
        $list = CHtml::listData($posts, 'id', 'title');

        return $list;
    }

    public static function getUserList()
    {
        $users = User::model()->findAll();
        $list = CHtml::listData($users, 'id', 'username');

        return $list;
    }

    protected function beforeSave() {

        if (parent::beforeSave())
        {


            return true;
        }
        else
            return false;
    }

    public function beforeValidate() {

        if ( $this->isNewRecord and $this->scenario == 'addComment')
        {
            $this->date_create = date('Y-m-d H:i:s');
            $this->status = self::STATUS_ACTIVE;
            $this->user_id = Yii::app()->user->id;
        }

        return parent::beforeValidate();
    }
}
