<?php

/**
 * This is the model class for table "tbl_post".
 *
 * The followings are the available columns in table 'tbl_post':
 * @property integer $id
 * @property string $title
 * @property string $short_content
 * @property string $content
 * @property integer $category_id
 * @property integer $status
 * @property string $pub_date
 */
class Post extends CActiveRecord
{

    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $post_images = array();

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, category_id, status, pub_date, short_content, content', 'required'),
			array('category_id, status', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('short_content, content, pub_date, post_images', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, short_content, content, category_id, status, pub_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'images' => array(self::HAS_MANY, 'PostImage', 'post_id'),
            'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=> 'comments.status='.Comment::STATUS_ACTIVE),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'commentCount' => array(self::STAT, 'Comment', 'post_id',
                'condition'=>'status='.Comment::STATUS_ACTIVE),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'short_content' => 'Краткое описание',
			'content' => 'Текст',
			'category_id' => 'Category',
			'status' => 'Статус',
			'pub_date' => 'Дата публикации',
            'post_images' => 'Картинки к посту'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('short_content',$this->short_content,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('pub_date',$this->pub_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public static function getStatusList()
    {
        return array(
            self::STATUS_DEACTIVE => 'Не активно',
            self::STATUS_ACTIVE => 'Активно'
        );

    }

    public static function getStatus($numer)
    {
        $list = self::getStatusList();

        if ( isset($list[$numer]) )
            return $list[$numer];

        return null;
    }

    protected function afterSave() {

	    parent::afterSave();


    }

    protected function deleteImages()
    {
        $images = $this->images;

        foreach ($images as $image)
        {
            if ( file_exists($image->image) )
                unlink($image->image);
        }


    }

    protected function beforeSave() {

        if (parent::beforeSave())
        {
            if ($this->isNewRecord)
              $this->user_id = Yii::app()->user->id;

            return true;
        }
        else
            return false;
    }


    protected function afterDelete()
    {
        parent::afterDelete();

        $this->deleteImages();
        PostImage::model()->deleteAll('post_id='.$this->id);


    }



}
