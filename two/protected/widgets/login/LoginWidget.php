<?php

class LoginWidget extends CWidget
{
    public function run()
    {
        $model = new LoginForm;

        $this->render('login', array('model'=>$model));
    }
}