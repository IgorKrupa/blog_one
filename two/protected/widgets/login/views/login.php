<?php if (Yii::app()->user->isGuest): ?>

<?php
Yii::app()->clientScript->registerScript('login_javascript', '
              
              $(document).ready(function(){
                            
                $(document).on("click", "#login-show-dialog", function (event) {
                    event.preventDefault();
                    $("#login-dialog").dialog("open");
                });
                
            });
            ', CClientScript::POS_END);

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'login-dialog',
    // additional javascript options for the dialog plugin
    'options'=>array(
        'title'=>'Авторизация',
        'autoOpen'=>false,
    ),
));

?>

<h1>Вход</h1>

<p>Пожалуйста, заполните следующую форму с вашими учетными данными для входа.</p>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'action' => array('site/login'),
        'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="row">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username'); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php echo $form->passwordField($model,'password'); ?>
        <?php echo $form->error($model,'password'); ?>
        <p class="hint">
            Hint: You may login with <kbd>demo</kbd>/<kbd>demo</kbd> or <kbd>admin</kbd>/<kbd>admin</kbd>.
        </p>
    </div>

    <div class="row rememberMe">
        <?php echo $form->checkBox($model,'rememberMe'); ?>
        <?php echo $form->label($model,'rememberMe'); ?>
        <?php echo $form->error($model,'rememberMe'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Login'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->

<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>

<?php endif; ?>
