<?php
class CategoryTreesWidget extends CWidget
{
    public function run()
    {

        $criteria=new CDbCriteria;
        $criteria->order='t.left';
        $criteria->addCondition('t.status', Category::STATUS_ACTIVE);

        $categories=Category::model()->findAll($criteria);

        $this->render('tree', array('categories'=>$categories));
    }

    protected function registerScripts()
    {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js/jquery-treeview/jquery.treeview.js');

        $cs->registerScript('tree_javascript', '

              $(document).ready(function(){
                    $("#trees").treeview({
                        persist: "location",
                        collapsed: true,
                        unique: true
                    });

            });
            ', CClientScript::POS_END);

    }


}
