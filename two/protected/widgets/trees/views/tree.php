
<script type="text/javascript" src="/js/jquery-treeview/jquery.treeview.js"></script>

<div>
    <ul id="trees" style="display: inline-block;">
    <?php echo $tree; ?>
    </ul>
</div>

<?php
Yii::app()->clientScript->registerScript('tree_javascript', '

              $(document).ready(function(){
                    $("#trees").treeview({
                        persist: "location",
                        collapsed: true,
                        unique: true
                    });

            });
            ', CClientScript::POS_END);


?>
