<?php
class RegistrationWidget extends CWidget
{
    public function run()
    {
        $model = new User;
        $model->scenario='register';

        $this->render('registration', array('model'=>$model));
    }

}