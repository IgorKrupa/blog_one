<?php if (Yii::app()->user->isGuest): ?>

<?php
    Yii::app()->clientScript->registerScript('registration_javascript', '
              
              $(document).ready(function(){
                            
                $(document).on("click", "#registration-show-dialog", function (event) {
                    event.preventDefault();
                    $("#registration-dialog").dialog("open");
                });
                
            });
            ', CClientScript::POS_END);

    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id'=>'registration-dialog',
        // additional javascript options for the dialog plugin
        'options'=>array(
            'title'=>'Регистрация',
            'autoOpen'=>false,
        ),
    ));

?>

<h1>Регистрация</h1>

<p>Пожалуйста, заполните следующую форму с вашими учетными данными для регистрации аккаунта.</p>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'registration-form',
        'action' => array('site/registration'),
        'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <p class="note">Поля с <span class="required">*</span> обязательные.</p>

    <div class="row">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username'); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php echo $form->passwordField($model,'password'); ?>
        <?php echo $form->error($model,'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'password_repeat'); ?>
        <?php echo $form->passwordField($model,'password_repeat'); ?>
        <?php echo $form->error($model,'password_repeat'); ?>
    </div>

    <?php if(CCaptcha::checkRequirements()): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'verifyCode'); ?>
            <div>
                <?php $this->widget('CCaptcha'); ?>
                <?php echo $form->textField($model,'verifyCode'); ?>
            </div>
            <div class="hint">Please enter the letters as they are shown in the image above.
                <br/>Letters are not case-sensitive.</div>
            <?php echo $form->error($model,'verifyCode'); ?>
        </div>
    <?php endif; ?>



    <div class="row buttons">
        <?php echo CHtml::submitButton('Регистрация'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->


<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>

<?php endif; ?>