<?php

class m170118_091111_create_category_table extends CDbMigration
{
	public function up()
	{
	    $this->createTable('tbl_category', array(
            'id'=>'pk',
            'title'=>'string NOT NULL',
            'status'=>'integer',
            'left'=>'integer NOT NULL',
            'right'=>'integer NOT NULL',
            'level'=>'integer NOT NULL',
        ));
	}

	public function down()
	{
        $this->dropTable('tbl_category');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}