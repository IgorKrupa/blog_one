<?php

class m170118_091226_create_post_image_table extends CDbMigration
{
	public function up()
	{
	    $this->createTable('tbl_post_image', array(
	        'id'=>'pk',
            'image'=>'string NOT NULL',
            'post_id'=>'integer NOT NULL'
        ));
	}

	public function down()
	{
        $this->dropTable('tbl_post_image');

		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}