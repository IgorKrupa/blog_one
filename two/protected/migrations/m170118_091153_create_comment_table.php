<?php

class m170118_091153_create_comment_table extends CDbMigration
{
	public function up()
	{
	    $this->createTable('tbl_comment', array(
	        'id'=>'pk',
            'post_id' => 'integer NOT NULL',
            'content'=>'text NOT NULL',
            'user_id'=>'integer NOT NULL',
            'date_create'=>'datetime',
            'status' => 'integer NOT NULL'
        ));
	}

	public function down()
	{
        $this->dropTable('tbl_comment');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}