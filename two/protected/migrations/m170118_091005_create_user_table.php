<?php

class m170118_091005_create_user_table extends CDbMigration
{
	public function up()
	{
	    $this->createTable('tbl_user', array(
	        'id' => 'pk',
            'username' => 'string NOT NULL',
            'email' => 'string NOT NULL',
            'status' => 'integer',
            'role' => 'string NOT NULL',
            'password' => 'string NOT NULL',
            'password_salt' => 'string NOT NULL',
            'datetime_registration' => 'datetime'
        ));

	}

	public function down()
	{
		$this->dropTable('tbl_user');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}