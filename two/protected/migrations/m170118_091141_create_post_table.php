<?php

class m170118_091141_create_post_table extends CDbMigration
{
	public function up()
	{
	    $this->createTable('tbl_post', array(
	        'id'=>'pk',
            'title'=>'string NOT NULL',
            'short_content'=>'text',
            'content'=>'text',
            'category_id'=>'integer NOT NULL',
            'status'=>'integer NOT NULL',
            'pub_date'=>'datetime'
        ));
	}

	public function down()
	{
		$this->dropTable('tbl_post');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}