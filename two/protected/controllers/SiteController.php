<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $this->layout='column2';

		$criteria=new CDbCriteria(array(
		        'condition'=>'t.status='.Post::STATUS_ACTIVE
                    .' AND category.status='.Category::STATUS_ACTIVE
                    .' AND t.pub_date <= NOW()',
                'order'=>'t.pub_date DESC',
                'with'=>array('commentCount', 'user', 'category')
            ));

		if (isset($_GET['category']))
        {
            $category = (int) $_GET['category'];
            $ids = $this->getCatIds($category);

            $criteria->addInCondition('t.category_id', $ids);
        }

        $dataProvider = new CActiveDataProvider('Post', array(
            'pagination'=>array(
                'pageSize'=>10
            ),
            'criteria'=>$criteria
        ));

		$this->render('index', array(
		    'dataProvider'=>$dataProvider
        ));
	}

	protected function getCatIds($category)
    {
        $ids=array($category);
        $category=Category::model()->findByPk($category);
        $descendants=$category->descendants()->findAll(array('condition'=>'t.status='.Category::STATUS_ACTIVE));

        foreach ($descendants as $row)
            $ids[] = $row->id;

        return $ids;
    }

	public function actionView($id)
    {


        $condition='';

        if (Yii::app()->user->isGuest)
            $condition='t.status='.Post::STATUS_ACTIVE;


        $model=Post::model()->findByPk($id, $condition);

        if ($model===null)
            throw new CHttpException(404, 'Страница не найдена');

        $comments = Comment::model()->findAllByAttributes(array('post_id'=>$id, 'status'=>Comment::STATUS_ACTIVE), array('order'=>'date_create DESC'));

        $comment = new Comment;
        $comment->post_id=$model->id;

        $this->render('view', array('model'=>$model, 'comment'=>$comment, 'comments'=>$comments));
    }

    //добавление комментария аяксом
    public function actionAddComment()
    {
        $model=new Comment('addComment');
        $data=array('status'=>'fail');

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='comment-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Comment']))
        {
            $model->attributes=$_POST['Comment'];

            if ($model->save())
            {
                $data['status']='ok';
                $data['date_create'] = date('F j, Y \a\t h:i a', strtotime($model->date_create));
                $data['user']= $model->user->username;
                $data['content']=$model->content;
            }
        }

        echo CJSON::encode($data);
        Yii::app()->end();
    }


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

    // генерация форми аяксом
    public function actionGetLoginForm()
    {
        $this->OffJqueryScript();

        $model=new LoginForm;
        $form = $this->renderPartial('login',array('model'=>$model), false, true);

        echo $form;
    }

    public function actionGetRegistrationForm()
    {
        $this->OffJqueryScript();

        $model = new User;
        $model->scenario='register';
        $form = $this->renderPartial('registration', array('model'=>$model), false, true);

        echo $form;
    }

    protected function OffJqueryScript()
    {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false; // отключение повторной загрузки скрипта
        Yii::app()->clientScript->scriptMap['jquery-ui.js'] = false; // отключение повторной загрузки скрипта
        Yii::app()->clientScript->scriptMap['jquery-ui.min.js'] = false; // отключение повторной загрузки скрипта
    }

    public function actionRegistration()
    {
        $model = new User;
        $model->scenario='register';

        if ( isset($_POST['ajax']) and $_POST['ajax'] == 'registration-form' )
        {
            $model->setScenario('ajax');
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['User']))
        {

            $model->setScenario('ajax');
            $model->attributes = $_POST['User'];
            $password = $model->password;

            if ($model->validate() && $model->save())
            {
                $identity=new UserIdentity($model->username, $password);

                if ($identity->authenticate())
                {
                    Yii::app()->user->login($identity);
                    $this->redirect(Yii::app()->user->returnUrl);
                }

            }

        }

        $this->redirect(Yii::app()->user->returnUrl);
    }

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
