<?php

class DefaultController extends Controller
{
    public $layout='//layouts/column2';

  public function filters()
   {
       return array(
           'accessControl',
       );
   }

    public function accessRules()
    {
        return array(

            array('allow',
                        // 'actions'=>array('index'),
                         'roles'=>array('admin'),
                     ),
            array('deny',
                //'actions'=>array('index'),
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->render('index');
    }


}
