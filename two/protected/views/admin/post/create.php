<?php
/* @var $this PostController */
/* @var $model Post */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление записями'=>array('admin'),
	'Создать запись',
);

$this->menu=array(
	array('label'=>'Управление записями', 'url'=>array('admin')),
);
?>

<h1>Создать запись</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>