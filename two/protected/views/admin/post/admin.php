<?php
/* @var $this PostController */
/* @var $model Post */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление записями',
);

$this->menu=array(
	array('label'=>'Создать запись', 'url'=>array('create')),
);

?>

<h1>Управление записями</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'post-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'afterAjaxUpdate'=>"function() { 
        jQuery('#Post_pub_date').datepicker(jQuery.extend(jQuery.datepicker.regional['ru'],{'showAnim':'fold','dateFormat':'yy-mm-dd','changeMonth':'true','showButtonPanel':'true','changeYear':'true'})); 
    }",
    'columns'=>array(
		'id',
		'title',
        array(
            'name' => 'category_id',
            'value' => 'Category::getCategoryName($data->category_id)',
            'filter' => Category::getCategoriesList()
        ),
        array(
            'name' => 'status',
            'value' => 'Post::getStatus($data->status)',
            'filter' => Post::getStatusList()
        ),


        array(
            'name'=>'pub_date',
            'type'=>'raw',
            'filter'=>false,
            'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,
                'attribute'=>'pub_date',
                'language'=>'ru',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=>'yy-mm-dd',
                    'changeMonth' => 'true',
                    'changeYear'=>'true',
                    'showButtonPanel' => 'true',
                ),
            ),true),
            'htmlOptions' => array('style' => 'width:130px;'),
        ),



        array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
