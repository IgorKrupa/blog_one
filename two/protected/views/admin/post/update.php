<?php
/* @var $this PostController */
/* @var $model Post */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление записями'=>array('admin'),
	'Редактирование записи',
);

$this->menu=array(
	array('label'=>'Создать запись', 'url'=>array('create')),
	array('label'=>'Управление записями', 'url'=>array('admin')),
);
?>

<h1>Редактирование записи <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>