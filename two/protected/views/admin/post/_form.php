<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */
?>

<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'post-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Поля с <span class="required">*</span> обязательные.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'short_content'); ?>
		<?php echo $form->textArea($model,'short_content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'short_content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50,'class'=>'ckeditor')); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
        <?php echo $form->dropDownList($model, 'category_id', Category::getCategoriesList()) ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model, 'status', Post::getStatusList()) ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pub_date'); ?>

        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'language'=>'ru',
            'model'=>$model,
            'attribute'=>'pub_date',
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=>'yy-mm-dd',
            ),

        )); ?>
		<?php echo $form->error($model,'pub_date'); ?>
	</div>

<?php if (!$model->isNewRecord): ?>
    <br />
    <div id="images">
        <ul>
        <?php foreach ($model->images as $image): ?>
            <li>
                <img src="<?= $image->image ?>" height="150px;" style="">
                <a class="image" data-image_id="<?= $image->id; ?>">Удалить</a>
            </li>
        <?php endforeach; ?>
        </ul>
    </div>
    <br />
    <div class="row">
        <a href="#" id="image-upload">Загрузить картинку</a>
        <input type="file" id="image-uploader" data-url="/admin/post/uploadImage" style="display: none;" name="post_images" />
    </div>
    <br />
<?php endif; ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
Yii::app()->clientScript->registerScript('post_form_javascript', '
              
              $(document).ready(function(){
              
                $(document).on("click", "#images ul li>a", function(event){
                    event.preventDefault();
                    var id = $(this).data("image_id");
                    var el = $(this).parent("li");
                    
                    $.post("/admin/post/deleteImage", {"id":id}, function(resp){
                        resp = jQuery.parseJSON(resp);
                        if ( resp.status == "ok" )
                            el.remove();
                    });
                });
                            
                $(document).on("click", "#image-upload", function (event) {
                    event.preventDefault();
                    $("input[type=\'file\'").trigger("click");
                });
                
                
                $("#image-uploader").change(function(){
                    var send_url = $(this).attr("data-url");
                    var fd = new FormData();
                
                    fd.append("post_images", this.files[0]);
                    fd.append("post_id", "'.$model->id.'");
                
                    $.ajax({
                        url: send_url,
                        type: "POST",
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function(resp){
                            resp = jQuery.parseJSON(resp);
                            if(resp.status == "ok"){
                                $("#images ul").append("<li><img src=\""+resp.image+"\" height=\"150px;\" /><a href=\"#\" data-image_id=\""+resp.id+"\">Удалить</a></li>"); 
                                
                                var fileInput = $("#image-uploader");
                                fileInput.replaceWith(fileInput.val("").clone(true));
                                
                           }
                           
                        }
                    });
                });
                
            });
            ', CClientScript::POS_END);

?>