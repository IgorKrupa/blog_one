<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление пользователями',
);

$this->menu=array(
	array('label'=>'Список пользователей', 'url'=>array('admin')),
	array('label'=>'Создать пользователя', 'url'=>array('create')),
);

?>

<h1>Управление пользователями</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'username',
        'email',
        array(
            'name' => 'role',
            'value'=>'$data->role',
            'filter'=>User::getRoleList()
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
