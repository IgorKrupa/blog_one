<?php
/* @var $this CommentController */
/* @var $model Comment */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление пользователями'=>array('admin'),
	'Создать пользователя',
);

$this->menu=array(
	array('label'=>'Управление пользователями', 'url'=>array('admin')),
);
?>

<h1>Создать пользователя</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>