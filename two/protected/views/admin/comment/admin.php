<?php
/* @var $this CommentController */
/* @var $model Comment */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление комментариями',
);

$this->menu=array(
    array('label'=>'Создать комментарий', 'url'=>array('create')),
	array('label'=>'Управление комментариями', 'url'=>array('admin')),
);

?>

<h1>Управление комментариями</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comment-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array(
            'name'=>'username_search',
            'value'=>'$data->user->username',
            //'filter'=>
        ),
		'content',
        array(
            'name'=>'post_search',
            'value'=>'$data->post->title'
        ),
        'date_create',
        array(
            'name' => 'status',
            'value' => 'Comment::getStatus($data->status)',
            'filter' => Comment::getStatusList()
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
