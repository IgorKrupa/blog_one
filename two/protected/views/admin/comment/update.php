<?php
/* @var $this CommentController */
/* @var $model Comment */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление комментариями'=>array('admin'),
	'Редактирование комментария',
);

$this->menu=array(
	array('label'=>'Создать комментарий', 'url'=>array('create')),
	array('label'=>'Управление комментариями', 'url'=>array('admin')),
);
?>

<h1>Редактирование комментария <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>