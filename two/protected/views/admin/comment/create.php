<?php
/* @var $this CommentController */
/* @var $model Comment */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление комментариями'=>array('admin'),
	'Создать комментарий',
);

$this->menu=array(
	array('label'=>'Управление комментариями', 'url'=>array('admin')),
);
?>

<h1>Создать комментарий</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>