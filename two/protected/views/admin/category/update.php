<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление категориями'=>array('admin'),
	'Редактирование категории',
);

$this->menu=array(
	array('label'=>'Создать категорию', 'url'=>array('create')),
	array('label'=>'Управление категориями', 'url'=>array('admin')),
);
?>

<h1>Редактирование категории <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>