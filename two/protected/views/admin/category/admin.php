<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление категориями',
);

$this->menu=array(
	array('label'=>'Список категорий', 'url'=>array('admin')),
	array('label'=>'Создать категорию', 'url'=>array('create')),
);

?>

<h1>Управление категориями</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'title',
        array(
                'name' => 'status',
                'value' => 'Category::getStatus($data->status)',
                'filter' => Category::getStatusList()
        ),
		array(
		        'name' => 'parent_id',
                'value' => 'Category::getCategoryName($data->parent_id)',
                'filter' => Category::getCategoriesList()
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
