<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
    'Админ панель'=>array('/admin'),
	'Управление категориями'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Управление категориями', 'url'=>array('admin')),
);
?>

<h1>Создать категорию</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>