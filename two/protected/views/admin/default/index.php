<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
    'Админ панель',
);

$this->menu=array(
    array('label'=>'Управление категориями', 'url'=>array('admin/category/admin')),
    array('label'=>'Управление записями', 'url'=>array('admin/post/admin')),
    array('label'=>'Управление комментариями', 'url'=>array('admin/comment/admin')),
    array('label'=>'Управление пользователями', 'url'=>array('admin/user/admin')),
);

?>

<h1>Админ панель</h1>

<ul>
    <li><?php echo CHtml::link('Управление категориями', array('admin/category/admin')); ?></li>
    <li><?php echo CHtml::link('Управление записями', array('admin/post/admin')); ?></li>
    <li><?php echo CHtml::link('Управление комментариями', array('admin/comment/admin')); ?></li>
    <li><?php echo CHtml::link('Управление пользователями', array('admin/user/admin')); ?></li>
</ul>