<!-- Add fancyBox -->
<link rel="stylesheet" href="/js/fancyBox/source/jquery.fancybox.css?v=2.1.6" type="text/css" media="screen" />
<script type="text/javascript" src="/js/fancyBox/source/jquery.fancybox.pack.js?v=2.1.6"></script>

<div class="post">
    <div class="title">
        <?php echo CHtml::link(CHtml::encode($data->title), array('post/view', 'id'=>$data->id)); ?>
    </div>
    <div class="author">
        Опубликовано <?php echo $data->user->username. ' - ' . date('F j, Y', strtotime($data->pub_date)); ?>
    </div>
    <div class="content">
        <?php
        $this->beginWidget('CMarkdown', array('purifyOutput'=>true));
        echo $data->content;
        $this->endWidget();
        ?>
    </div>

    <div class="images">
        <?php foreach ($data->images as $image): ?>

            <a class="fancybox" rel="group" href="<?= $image->image ?>"><img src="<?= $image->image ?>" height="150px;" style=""></a>
        <?php endforeach; ?>

    </div>

    <div class="nav">
        <br />
        <?php echo CHtml::link("Комментарии ({$data->commentCount})", array('post/view', 'id'=>$data->id, '#comments')); ?> |
        Опубликовано <?php echo date('F j, Y', strtotime($data->pub_date)); ?>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('login_javascript', '
              
              $(document).ready(function() {
                    $(".fancybox").fancybox();
              });
            ', CClientScript::POS_END);

?>

<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>