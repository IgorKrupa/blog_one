<?php
/* @var $this CommentController */
/* @var $model Comment */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'comment-form',
        'action' => array('site/addComment'),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>true,

        'clientOptions' => array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>true,
            'validateOnType'=>false,
        ),


    )); ?>

    <p class="note">Поля с <span class="required">*</span> обязательные.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'content'); ?>
        <?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'content'); ?>
    </div>

    <?php echo $form->hiddenField($model, 'post_id'); ?>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Отправить'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<?php
Yii::app()->clientScript->registerScript('addComment_javascript', '
              
              $(document).ready(function(){
                            
                $(document).on("submit", "#comment-form", function (event) {
                    event.preventDefault();
                    data = $(this).serialize();
                    $.post("/site/addComment", data, function(resp){
                        console.log(resp);
                        var data = jQuery.parseJSON( resp );
                        var el = "<div class=\"comment\"><div class=\"author\">"+data.user+" Написал:</div><div class=\"time\">"+data.date_create+"</div><div class=\"content\">"+data.content+"</div></div>";
                        $("#comments").prepend(el);
                        $("#comment-form")[0].reset();
                    });
                });
                
            });
            ', CClientScript::POS_END);


?>