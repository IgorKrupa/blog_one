<div class="post">
    <div class="title">
        <?php echo CHtml::link(CHtml::encode($data->title), array('post/view', 'id'=>$data->id)); ?>
    </div>
    <div class="author">
        Опубликовано <?php echo $data->user->username. ' - ' . date('F j, Y', strtotime($data->pub_date)); ?>
    </div>
    <div class="content">
        <?php
        $this->beginWidget('CMarkdown', array('purifyOutput'=>true));
        echo $data->short_content;
        $this->endWidget();
        ?>
    </div>
    <div class="nav">
        <?php echo CHtml::link("Комментарии ({$data->commentCount})", array('post/view', 'id'=>$data->id)); ?>

    </div>
</div>