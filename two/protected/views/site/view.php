<?php
$this->breadcrumbs=array(
    $model->title,
);
$this->pageTitle=$model->title;
?>

<?php $this->renderPartial('_viewPost', array(
    'data'=>$model,
)); ?>

<?php if($model->commentCount>=1): ?>
    <h3>
        <?php echo $model->commentCount>1 ? $model->commentCount . ' комментария' : '1 комментарий'; ?>
    </h3>
<?php endif; ?>

<div id="comments">

        <?php $this->renderPartial('_comments',array(
            'post'=>$model,
            'comments'=>$comments,
        )); ?>


    <?php if (!Yii::app()->user->isGuest): ?>
        <br />
        <h3>Оставить комментарий</h3>

        <?php if(Yii::app()->user->hasFlash('commentSubmitted')): ?>
            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('commentSubmitted'); ?>
            </div>
        <?php else: ?>
            <?php $this->renderPartial('_formComment',array(
                'model'=>$comment,
            )); ?>
        <?php endif; ?>

    <?php endif; ?>

</div>
