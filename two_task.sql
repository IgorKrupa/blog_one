-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 24 2017 г., 12:54
-- Версия сервера: 5.5.54-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `two_task`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `left` int(11) NOT NULL,
  `right` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `parent_id` int(10) unsigned NOT NULL COMMENT 'родительская категория ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `title`, `status`, `left`, `right`, `level`, `root`, `parent_id`) VALUES
(7, 'Первая категория ', 1, 1, 6, 1, 7, 0),
(9, 'Вторая категория', 1, 2, 5, 2, 7, 7),
(12, 'Третья', 1, 3, 4, 3, 7, 9);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_comment`
--

CREATE TABLE IF NOT EXISTS `tbl_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_create` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tbl_comment`
--

INSERT INTO `tbl_comment` (`id`, `post_id`, `content`, `user_id`, `date_create`, `status`) VALUES
(1, 1, '132', 15, '2017-01-22 14:49:43', 1),
(2, 2, 'dfrgdrt', 15, '2017-01-23 14:49:05', 1),
(3, 2, 'gfhgfh', 15, '2017-01-23 14:49:19', 0),
(4, 2, '222', 15, '2017-01-23 14:49:23', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1484733473),
('m170118_091005_create_user_table', 1484733475),
('m170118_091111_create_category_table', 1484733475),
('m170118_091141_create_post_table', 1484733475),
('m170118_091153_create_comment_table', 1484733475),
('m170118_091226_create_post_image_table', 1484733475);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_post`
--

CREATE TABLE IF NOT EXISTS `tbl_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `short_content` text,
  `content` text,
  `category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `pub_date` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_post`
--

INSERT INTO `tbl_post` (`id`, `title`, `short_content`, `content`, `category_id`, `status`, `pub_date`, `user_id`) VALUES
(1, 'u8jjuy', 'ujuyj', '<p>yujyuj</p>\r\n', 12, 1, '2017-01-24', 15);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_post_image`
--

CREATE TABLE IF NOT EXISTS `tbl_post_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_salt` varchar(255) NOT NULL,
  `datetime_registration` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `email`, `status`, `role`, `password`, `password_salt`, `datetime_registration`) VALUES
(13, '123123', '123@gmail.com', 1, 'user', '$2y$13$1rAvDApVFrd.ZO40ENHjxOXwrwMVXj9fgU5Cb.7hvcoMSM5WD/PGq', '$2y$13$jAvtR9GH8kggME4WMO6Ini', '2017-01-19 09:44:52'),
(14, '111', '123@gmail.com', 1, 'user', '$2y$13$dHcLzvlibcAwLbXUJOSsHuhBMTNaM16icc1WiADdnSNgp5Hx5eiP6', '$2y$13$CA3ywlJ90JDTJFWr/VHnev', '2017-01-19 09:54:26'),
(15, 'admin', 'admin@gmail.com', 1, 'admin', '$2y$13$./pCZVwaRizxixPCHvz1HOd5CVCk2j7o9g17M9DZKNQAlHidRSflK', '$2y$13$SypjeTCqSyrZ8chB1/Uo/L', '2017-01-19 11:04:50'),
(16, '1111', '123@gmail.com', 1, 'user', '$2y$13$OmMruTGBZp4K0IMOnkFwvOURdZHMcRx8H75XnNU.yy/75CInbXJkm', '$2y$13$NsOsyyFE7iGdp1Y2FmviKu', '2017-01-19 11:07:11'),
(17, '11111', '123@gmail.com', 1, 'user', '$2y$13$GjG7PU10JkSzgzl6Fd3Hz.O2FO8vpScAwO7G5L1WyVVD38I6ir/eu', '$2y$13$Ca5Y8dWvrVhNnvguI4Nq3/', '2017-01-19 11:10:31'),
(18, 'ddd', '123@gmail.com', 1, 'user', '$2y$13$mMWfw3LKgNT2i2fuMQaLDeP10gJ6kecUVohvKna6T0IDRzsOFVnaa', '$2y$13$.tjaq6e8WOVAdEswOQB492', '2017-01-19 11:14:54'),
(19, 'aqaq', '123123@gmail.copm', 1, 'user', '$2y$13$hIBs93yrcLagrykIGmbwpe7xkmJygvh7BxCZspZJuKGr5.UyOliDG', '$2y$13$Wr9InRSmXyOpBdMwblFL4g', '2017-01-22 14:54:48'),
(20, '2121', '123123@gmail.copm', 1, 'user', '$2y$13$2Xd1YSRsCKneLx3wJptWU.UgH7t.Tc.3nXZFQEHGpIHuYykvb6jyq', '$2y$13$7AKKPl0jCyC8GYj4IuVrVP', '2017-01-22 14:56:26'),
(21, 'test', '123123@gmail.copm', 1, 'user', '$2y$13$Eq8sQSoxvPGcpdW7F/dJouZ8d5mfYtjaMG424VKIWLGcR4V/BJ28q', '$2y$13$LUGyxfqvQqRRzLAWprlrf7', '2017-01-22 15:53:16'),
(22, 'tyhtytyy', '123@gmail.com', 1, 'user', '$2y$13$x4.HeDsLi2Z2Uws6oLX0ZOccZkEzsnQdAjTq.xVhL.HjlmzItledm', '$2y$13$iu/POJ0WiCNbCJplqIGjK9', '2017-01-23 16:03:40');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
